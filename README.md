# helm-chart
Demo Helm chart to deploy! 

## Getting started
To deploy this helm chart to your cluster, run the following commands

```
helm repo add helm-demo https://gitlab.com/api/v4/projects/52962113/packages/helm/stable
helm repo update
helm upgrade --install helm-demo helm-demo/goweb --values values.yaml

```

## Notes

If you see any permission issues running helm repo add , please do the following 
```
helm repo add --username <username> --password <token>  helm-demo https://gitlab.com/api/v4/projects/52962113/packages/helm/stable

```
